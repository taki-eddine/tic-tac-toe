//************************************************************************************************//
#include "ttt/Game.hpp"
//************************************************************************************************//

////////////////////////////////////////////////////////////
int main(int argc, char* argv[]) {
    auto app  = Gtk::Application::create(argc, argv, "org.gtkmm.tic-tac-toe");
    auto game = ttt::Game();

    return app->run(game);
}
