#include "ttt/model/Cell.hpp"

//************************************************************************************************//
//************************************************************************************************//

namespace ttt {
namespace model {

////////////////////////////////////////////////////////////
Cell::Cell() {
}


////////////////////////////////////////////////////////////
Cell& Cell::operator=(Tag tag) {
    _tag = tag;
    return *this;
}


////////////////////////////////////////////////////////////
bool operator==(const Cell& right, const Cell& left) {
    return right._tag == left._tag;
}


////////////////////////////////////////////////////////////
bool operator==(const Cell& right, const Tag tag) {
    return right._tag == tag;
}


////////////////////////////////////////////////////////////
bool operator!=(const Cell& right, const Cell& left) {
    return right._tag != left._tag;
}


////////////////////////////////////////////////////////////
bool operator!=(const Cell& right, const Tag tag) {
    return right._tag != tag;
}

}} // ::ttt::cmp
