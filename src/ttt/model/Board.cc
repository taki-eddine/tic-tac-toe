#include "ttt/model/Board.hpp"
#include "ttt/GameState.hpp"

//************************************************************************************************//
//************************************************************************************************//

namespace ttt {
namespace model {

////////////////////////////////////////////////////////////
Board::Board() {
}


////////////////////////////////////////////////////////////
char Board::getNBlanks() {
    return _nblanks;
}


////////////////////////////////////////////////////////////
std::vector< std::pair<unsigned, unsigned> > Board::getAvailableMoves() {
    std::vector< std::pair<unsigned, unsigned> > moves;

    for (unsigned i = 0; i < 3; i++) {
        for (unsigned j = 0; j < 3; j++) {
            if (_cells[i][j] == Tag::BLANK) {
                moves.push_back(std::make_pair(i, j));
            }
        }
    }

    return moves;
}


////////////////////////////////////////////////////////////
GameState Board::check(Tag tag, const std::pair<unsigned, unsigned>& move) {
    _cells[move.first][move.second] = tag;
    _nblanks--;

    return _isWinMove(move);
}


////////////////////////////////////////////////////////////
void Board::reset(unsigned i, unsigned j) {
    _cells[i][j] = Tag::BLANK;
    _nblanks++;
}


////////////////////////////////////////////////////////////
void Board::reset() {
    for (unsigned i = 0; i < 3; i++) {
        for (unsigned j = 0; j < 3; j++) {
            _cells[i][j] = Tag::BLANK;
        }
    }

    _nblanks = 9;
}


// -- Private ----------------------------------------------------------------------------------- //


////////////////////////////////////////////////////////////
Cell& Board::operator()(unsigned i, unsigned j) {
    return _cells[i][j];
}


////////////////////////////////////////////////////////////
GameState Board::_isWinMove(const std::pair<unsigned, unsigned>& move) const {
    // check for a win.
    // horizontal check.
    if ( (_cells[move.first][0] == _cells[move.first][1])
      && (_cells[move.first][0] == _cells[move.first][2]) ) {
        return GameState::WIN;
    }

    // vertical check.
    if ( (_cells[0][move.second] == _cells[1][move.second])
      && (_cells[0][move.second] == _cells[2][move.second]) ) {
        return GameState::WIN;
    }

    // main diagonal.
    if ( (move.first == move.second)
      && (_cells[0][0] == _cells[1][1])
      && (_cells[0][0] == _cells[2][2]) ) {
        return GameState::WIN;
    }

    // second diagonal.
    if ( (move.first + move.second == 2)
      && (_cells[0][2] == _cells[1][1])
      && (_cells[0][2] == _cells[2][0]) ) {
        return GameState::WIN;
    }

    // check for a draw.
    if (_nblanks == 0)
        return GameState::DRAW;

    return GameState::UNKNOWN;
}

}} // ::ttt::cmp
