#include "ttt/AiPlayer.hpp"

//************************************************************************************************//
#include <chrono>
#include <fmt/format.h>
using namespace std::chrono;
#include "ttt/model/Board.hpp"
#include "ttt/GameState.hpp"

#define USE_ALPHA_BETA true
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////////
AiPlayer::AiPlayer(const std::string& name, Tag tag) :
    Player(name, tag) {
}


////////////////////////////////////////////////////////////
void AiPlayer::move(Board& board) {
    _nodes = 0;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
#if USE_ALPHA_BETA == true
    _move = minimax(board, GameState::TOP_LOSE, GameState::TOP_WIN);
#else
    _move = minimax(board);
#endif
    high_resolution_clock::time_point t2 = high_resolution_clock::now();

    duration<double> time_span = duration_cast< duration<double> >(t2 - t1);

    fmt::print("AiPlayer calculating move time: {:f} second, {} nodes \n", time_span.count(), _nodes);
}


////////////////////////////////////////////////////////////
std::pair<unsigned, unsigned> AiPlayer::minimax(model::Board board, int alpha, int beta) {
    return max(board, alpha, beta).second;
}


////////////////////////////////////////////////////////////
std::pair<int, std::pair<unsigned, unsigned> > AiPlayer::max(model::Board& board, int alpha, int beta) {
    int score      = 0;
    int best_score = GameState::TOP_LOSE;
    GameState game_state = GameState::UNKNOWN;
    std::pair<unsigned, unsigned> best_move;
    std::vector< std::pair<unsigned, unsigned> > moves = board.getAvailableMoves();

    for (auto& move : moves) {
        game_state = board.check(Tag::O, move);

        // game over means either WIN or DRAW.
        if (game_state != GameState::UNKNOWN) {
            // game is over, get the score.
            score = (game_state == GameState::DRAW ? GameState::DRAW : GameState::WIN);
        }
        else {
            // game continue, opponent turns.
            score = min(board, alpha, beta).first;
        }

        // undo the last move to restore the board.
        board.reset(move.first, move.second);

        // compare with the new score with the best,
        // if it's better then this is the better move.
        if (score > best_score) {
            best_score = score;
            best_move  = std::make_pair(move.first, move.second);
        }

#if USE_ALPHA_BETA == true
        // check if our best score will be used by the parent (minimizer):
        if (best_score > beta) {
            // our value is `bigger` then `beta`, and we are searching to maximize
            // so any bigger value will not be used by our parent node
            // so prune and don't see the rest of childrens.
            break;
        }

        // our best score is still under `beta`, the parent will possible use our value,
        // so continue with the rest of children but with update `alpha`
        // (`aplha` will be update if `best_score` is higher).
        if (best_score > alpha) {
            alpha = best_score;
        }
#endif
    }

    _nodes++;

    return std::make_pair(best_score, best_move);
}


////////////////////////////////////////////////////////////
std::pair<int, std::pair<unsigned, unsigned> > AiPlayer::min(model::Board& board, int alpha, int beta) {
    int score      = 0;
    int best_score = GameState::TOP_WIN;
    GameState game_state = GameState::UNKNOWN;
    std::pair<unsigned, unsigned> best_move;
    std::vector< std::pair<unsigned, unsigned> > moves = board.getAvailableMoves();

    for (auto& move : moves) {
        // TODO: we must get the opponent `tag`.
        game_state = board.check(Tag::X, move);

        // game over means either WIN or DRAW.
        if (game_state != GameState::UNKNOWN) {
            // game is over, get the score.
            score = (game_state == GameState::DRAW ? GameState::DRAW : GameState::LOSE);
        }
        else {
            // game continue, opponent turns.
            score = max(board, alpha, beta).first;
        }

        // undo the last move to restore the board.
        board.reset(move.first, move.second);

        // compare with the new score with the best,
        // if it's better then this is the better move.
        if (score < best_score) {
            best_score = score;
            best_move  = std::make_pair(move.first, move.second);
        }

#if USE_ALPHA_BETA == true
        // check if our best score will be used by the parent (maximizer):
        if (best_score < alpha) {
            // our value is `less` then `alpha`, and we are searching to minimize
            // so any less value will not be used by our parent node
            // so prune and don't see the rest of childrens.
            break;
        }

        // our best score is still above `alpha`, the parent will possible use our value,
        // so continue with the rest of children but with update `beta`
        // (`beta` will be update if `best_score` is under).
        if (best_score < beta) {
            beta = best_score;
        }
#endif
    }

    _nodes++;

    return std::make_pair(best_score, best_move);
}

} // ::ttt
