#include "ttt/Game.hpp"

//************************************************************************************************//
#include <gtkmm/messagedialog.h>
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////////
Game::Game() {
    add(_board);
    show_all();
}


////////////////////////////////////////////////////////////
void Game::playersHumanVsAi(const std::pair<unsigned, unsigned>& human_move) {
    _p1->setMove(human_move);
    if (!play(_p1)) {
        _p2->move(_board);
        play(_p2);
    }
}


////////////////////////////////////////////////////////////
bool Game::play(std::unique_ptr<Player>& player) {
    _game_state = _board.check(player->getTag(), player->getMove());

    if (_game_state == GameState::WIN) {
        Gtk::MessageDialog(*this, player->getName() + " is the winner !!!").run();
        _board.reset();
        return true;
    }
    else if (_game_state == GameState::DRAW) {
        Gtk::MessageDialog(*this, "It's a draw !!!").run();
        _board.reset();
        return true;
    }

    return false;
}

} // ::ttt
