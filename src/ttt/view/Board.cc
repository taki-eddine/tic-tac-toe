#include "ttt/view/Board.hpp"

//************************************************************************************************//
//************************************************************************************************//

namespace ttt {
namespace view {


// ** Public ------------------------------------------------------------------------------------ //


////////////////////////////////////////////////////////////
Board::Board(Game& game) :
    _cells({
        {
            Cell(game, std::make_pair(0, 0)),
            Cell(game, std::make_pair(0, 1)),
            Cell(game, std::make_pair(0, 2))
        },
        {
            Cell(game, std::make_pair(1, 0)),
            Cell(game, std::make_pair(1, 1)),
            Cell(game, std::make_pair(1, 2))
        },
        {
            Cell(game, std::make_pair(2, 0)),
            Cell(game, std::make_pair(2, 1)),
            Cell(game, std::make_pair(2, 2))
        }
    }) {
    set_row_homogeneous(true);
    set_column_homogeneous(true);

    for (unsigned i = 0; i < 3; i++) {
        for (unsigned j = 0; j < 3; j++) {
            attach(_cells[i][j], j, i, 1, 1);
        }
    }
}


////////////////////////////////////////////////////////////
void Board::check(Tag tag, const std::pair<unsigned, unsigned>& move) {
    _cells[move.first][move.second] = tag;
}


////////////////////////////////////////////////////////////
void Board::reset() {
    for (unsigned i = 0; i < 3; i++) {
        for (unsigned j = 0; j < 3; j++) {
            _cells[i][j] = Tag::BLANK;
        }
    }
}


// ** Private ----------------------------------------------------------------------------------- //


////////////////////////////////////////////////////////////
Cell& Board::operator()(unsigned i, unsigned j) {
    return _cells[i][j];
}

}} // ::ttt::gui
