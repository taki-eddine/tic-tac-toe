#include "ttt/view/Cell.hpp"

//************************************************************************************************//
#include "ttt/view/Board.hpp"
#include "ttt/Game.hpp"
//************************************************************************************************//

namespace ttt::view {

////////////////////////////////////////////////////////////
Cell::Cell(ttt::Game& game, const std::pair<unsigned, unsigned>& id) :
    Gtk::Button() {
    signal_clicked().connect(
        sigc::bind< std::pair<unsigned, unsigned> >(
            sigc::mem_fun(&game, &ttt::Game::playersHumanVsAi),
            id
        )
    );
}


////////////////////////////////////////////////////////////
Cell& Cell::operator=(Tag tag) {
    switch (tag) {
        case Tag::X:
            *this = Gtk::Image("res/drawable/x_red.png");
            break;

        case Tag::O:
            *this = Gtk::Image("res/drawable/o_green.png");
            break;

        case Tag::BLANK:
            clear();
            break;
    }

    return *this;
}


// --  Private ----------------------------------------------------------------------------------- //


////////////////////////////////////////////////////////////
Cell& Cell::operator=(Gtk::Image&& image) {
    _image = std::move(image);
    set_image(_image);
    set_sensitive(false);

    return *this;
}


////////////////////////////////////////////////////////////
void Cell::clear() {
    _image.clear();
    set_sensitive(true);
}

} // ::ttt::gui
