#include "ttt/Board.hpp"

//************************************************************************************************//
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////
Board::Board(Game& game) :
    view::Board(game) {
}


////////////////////////////////////////////////////////
GameState Board::check(Tag tag, const std::pair<unsigned, unsigned>& move) {
    view::Board::check(tag, move);
    return model::Board::check(tag, move);
}


////////////////////////////////////////////////////////
void Board::reset() {
    view::Board::reset();
    model::Board::reset();
}

} // ::ttt
