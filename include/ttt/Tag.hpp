#pragma once

//************************************************************************************************//
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////////
enum class Tag {
    BLANK,
    X,
    O
};

} // ::ttt
