#pragma once

//************************************************************************************************//
#include <gtkmm/grid.h>
#include "ttt/view/Cell.hpp"

namespace ttt { class Game; }
//************************************************************************************************//

namespace ttt::view {

class Board : public Gtk::Grid {

public:
    ////////////////////////////////////////////////////////////
    Board(Game& game);

    ////////////////////////////////////////////////////////////
    void check(Tag tag, const std::pair<unsigned, unsigned>& move);

    ////////////////////////////////////////////////////////////
    void reset();

private:
    Cell _cells[3][3];

    ////////////////////////////////////////////////////////////
    Cell& operator()(unsigned i, unsigned j);

};

} // ::ttt::gui
