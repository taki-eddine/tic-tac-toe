#pragma once

//************************************************************************************************//
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include "ttt/Tag.hpp"

namespace ttt       { class Game;  }
namespace ttt::view { class Board; }
//************************************************************************************************//

namespace ttt::view {

////////////////////////////////////////////////////////////
class Cell : public Gtk::Button {

public:
    ////////////////////////////////////////////////////////////
    Cell(ttt::Game& game, const std::pair<unsigned, unsigned>& id);

    ////////////////////////////////////////////////////////////
    Cell& operator=(Tag tag);

private:
    Gtk::Image _image;

    ////////////////////////////////////////////////////////////
    Cell& operator=(Gtk::Image&& image);

    ////////////////////////////////////////////////////////////
    void clear();

};

} // ::ttt::view
