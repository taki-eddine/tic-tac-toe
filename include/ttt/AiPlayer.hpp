#pragma once

//************************************************************************************************//
#include <utility>
#include "ttt/Player.hpp"
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////////
class AiPlayer : public Player {
public:
    ////////////////////////////////////////////////////////////
    AiPlayer(const std::string& name, Tag tag);

    ////////////////////////////////////////////////////////////
    void move(Board& board) override;

    ////////////////////////////////////////////////////////////
    std::pair<unsigned, unsigned> minimax(model::Board board, int alpha = 0, int beta = 0);

    ////////////////////////////////////////////////////////////
    std::pair<int, std::pair<unsigned, unsigned> > max(model::Board& board, int alpha, int beta);

    ////////////////////////////////////////////////////////////
    std::pair<int, std::pair<unsigned, unsigned> > min(model::Board& board, int alpha, int beta);

private:
    int _nodes = 0;
};

} // ::ttt
