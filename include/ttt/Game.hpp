#pragma once

//************************************************************************************************//
#include "ttt/Board.hpp"
#include "ttt/HumanPlayer.hpp"
#include "ttt/AiPlayer.hpp"
#include "ttt/view/Game.hpp"
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////////
class Game : public view::Game {

public:
    ////////////////////////////////////////////////////////////
    Game();

    ////////////////////////////////////////////////////////////
    void playersHumanVsAi(const std::pair<unsigned, unsigned>& human_move);

    ////////////////////////////////////////////////////////////
    bool play(std::unique_ptr<Player>& player);

    ////////////////////////////////////////////////////////////
    void reset();

private:
    Board     _board      = Board(*this);
    GameState _game_state = GameState::UNKNOWN;
    std::unique_ptr<Player> _p1 = std::make_unique<HumanPlayer>("P1", Tag::X);
    std::unique_ptr<Player> _p2 = std::make_unique<AiPlayer>("AI", Tag::O);
    //std::unique_ptr<Player> _p2 = std::make_unique<HumanPlayer>("P2", Tag::O);

};

} // ::ttt
