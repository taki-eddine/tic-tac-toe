#pragma once

//************************************************************************************************//
#include <string>
#include <utility>
#include "ttt/Tag.hpp"
#include "ttt/Board.hpp"
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////////
class Player {

public:
    ////////////////////////////////////////////////////////////
    Tag getTag();

    ////////////////////////////////////////////////////////////
    std::string& getName();

    ////////////////////////////////////////////////////////////
    std::pair<unsigned, unsigned>& getMove();

    ////////////////////////////////////////////////////////////
    void setMove(const std::pair<unsigned, unsigned>& move);

    ////////////////////////////////////////////////////////////
    virtual void move(Board& board) = 0;

protected:
    std::string _name;
    Tag _tag;
    std::pair<unsigned, unsigned> _move;

    ////////////////////////////////////////////////////////////
    Player(const std::string& name, Tag tag);

};

} // ::ttt
