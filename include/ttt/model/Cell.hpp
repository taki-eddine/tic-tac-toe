#pragma once

//************************************************************************************************//
#include "ttt/Tag.hpp"
//************************************************************************************************//

namespace ttt {
namespace model {

////////////////////////////////////////////////////////////
class Cell {
    friend bool operator==(const Cell& right, const Cell& left);
    friend bool operator==(const Cell& right, const Tag tag);
    friend bool operator!=(const Cell& right, const Cell& left);
    friend bool operator!=(const Cell& right, const Tag tag);

public:
    ////////////////////////////////////////////////////////////
    Cell();

    ////////////////////////////////////////////////////////////
    Cell& operator=(Tag tag);

private:
    Tag _tag = Tag::BLANK;
};


////////////////////////////////////////////////////////////
bool operator==(const Cell& right, const Cell& left);

////////////////////////////////////////////////////////////
bool operator==(const Cell& right, const Tag tag);

////////////////////////////////////////////////////////////
bool operator!=(const Cell& right, const Cell& left);

////////////////////////////////////////////////////////////
bool operator!=(const Cell& right, const Tag tag);

}} // ::ttt::cmp
