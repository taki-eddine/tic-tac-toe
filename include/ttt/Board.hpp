#pragma once

//************************************************************************************************//
#include "ttt/view/Board.hpp"
#include "ttt/model/Board.hpp"
#include "ttt/GameState.hpp"

namespace ttt { class Game; }
//************************************************************************************************//

namespace ttt {

////////////////////////////////////////////////////////
class Board : public view::Board, public model::Board {

public:
    ////////////////////////////////////////////////////////
    Board(Game& game);

    ////////////////////////////////////////////////////////
    GameState check(Tag tag, const std::pair<unsigned, unsigned>& move);

    ////////////////////////////////////////////////////////
    void reset();

};

} // ::ttt
