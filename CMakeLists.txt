# ================== * Configuration * =================== #
# ::: Project options ::: #


# ================== * General Setup * =================== #
project(tic-tac-toe)
cmake_minimum_required(VERSION 3.8.2 FATAL_ERROR)

# ::: CMake properties ::: #
#include(cmake/utils.cmake)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")   # Path of 'shared' lib
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")   # Path of 'static' lib
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")   # Path of 'exec' file
set(RUNTIME_TESTS_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/tests") # Path of `test exec' file
set(CMAKE_INSTALL_PREFIX           "/usr/local")                # Path of 'install' prefix
set(CMAKE_EXPORT_COMPILE_COMMANDS  ON)

set(CMAKE_C_STANDARD          11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS        ON)
set(CMAKE_C_FLAGS             "")
set(CMAKE_C_FLAGS_DEBUG       "")
set(CMAKE_C_FLAGS_RELEASE     "")

set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        ON)
set(CMAKE_CXX_FLAGS             "")
set(CMAKE_CXX_FLAGS_DEBUG       "")
set(CMAKE_CXX_FLAGS_RELEASE     "")

set(CMAKE_AR     "gcc-ar")
set(CMAKE_RANLIB "gcc-ranlib")
set(CNAKE_NM     "gcc-nm")


# ==================== * Pre-Build * ===================== #
# ::: Packages ::: #
find_package(PkgConfig)

# -- GTKMM
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
link_directories(${GTKMM_LIBRARY_DIRS})
include_directories(${GTKMM_INCLUDE_DIRS})

# -- FMT
set(FMT_REQD_VERSION "4.0.0")
message(STATUS "Using static FMT ${FMT_REQD_VERSION} from 'extern'")
add_subdirectory("extern/fmt")

# ::: Include Folders ::: #
include_directories("${CMAKE_SOURCE_DIR}/include")
#header_directories()
#header_merge()
#directory_namespace()

# ::: Source Folders ::: #
file(GLOB_RECURSE SOURCE_FILES "${CMAKE_SOURCE_DIR}/src/*.c*")

# ::: Library Folders ::: #
link_directories("${CMAKE_BINARY_DIR}/lib")

# ::: Compiler options ::: #
set(C_COMMOM     "")
set(CXX_COMMON   "-Wno-invalid-offsetof -fno-pretty-templates")
set(C_CXX_COMMON "-Wall -Wextra -pipe")

set(LTO "-flto=8 -flto-compression-level=0 -flto-partition=none -fuse-linker-plugin -fno-fat-lto-objects")
set(PGO "-fprofile-use")
set(OPTIMIZE_OPTIONS "${LTO} -O3 -march=native -mtune=native")

set(DEBUG_OPTIONS "-g3 -gdwarf-4 -p -pg -fno-inline")

set(CMAKE_C_FLAGS_DEBUG   "${CMAKE_C_FLAGS_DEBUG}   ${C_COMMOM} ${C_CXX_COMMON} ${DEBUG_OPTIONS}")
set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} ${C_COMMOM} ${C_CXX_COMMON} ${OPTIMIZE_OPTIONS}")

set(CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG}   ${CXX_COMMON} ${C_CXX_COMMON} ${DEBUG_OPTIONS}")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${CXX_COMMON} ${C_CXX_COMMON} ${OPTIMIZE_OPTIONS}")


# ====================== * Build * ======================= #
add_custom_target(link_resources ALL
                  COMMAND ${CMAKE_COMMAND} -E create_symlink "${CMAKE_SOURCE_DIR}/res" "${CMAKE_BINARY_DIR}/bin/res")

add_executable(${PROJECT_NAME} ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES} "pthread" "X11" "fmt")
